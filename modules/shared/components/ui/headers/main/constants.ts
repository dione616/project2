export const menuItems = [
  { l: 'People (CSR)', h: '/people' },
  { l: 'Planets (SSR)', h: '/planets' },
  { l: 'Starships (SSG)', h: '/starships' }
];
